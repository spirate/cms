# Spirate CMS

Spirate cms development repository.

---

## Getting started

### Requisitos

* php >= 5.6.4
* composer ([official website](https://getcomposer.org))
* node ([official website](https://nodejs.org/en/download/))

### Steps

clonar el repositorio o [descargarlo](https://gitlab.com/spirate/cms/repository/archive.zip?ref=master).

```
git clone https://gitlab.com/spirate/cms.git
```

en la carpeta donde se clonó o descargó ejecutar el comando:

```
composer install
```

---

## Assets

para compilar assets junto con **laravel mix**, es necesario ejecutar el siguiente comando:

```
npm install
```

> Ahora puedes editar el fichero **webpack.mix.js** y ejecutar **laravel mix**

**ejecutar laravel mix**:

```
npm run dev
```

**ejecutar laravel mix observando cambios**

```
npm run watch
```

---

## semantic-ui

> [semantic website](http://semantic-ui.com)

**compilar css:**
```
npm run semantic-build-css
```

**compilar js:**
```
npm run semantic-build-js
```

**compilar todo (css, js)**
```
npm run semantic-build
```