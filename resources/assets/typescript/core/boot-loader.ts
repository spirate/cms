import { Vue } from 'av-ts'
import { Module } from './module'

export class Bootloader {
    /**
     * register modules
     *
     * @param modules
     */
    public static registerModules <T extends Module> (modules: { new (): T}[]): void {
        modules.forEach((module: { new (): T}) => {
            // instantiate this module
            new module();
        });
    }

    /**
     * register Vue
     */
    public static registerVueRoot (element: string): Vue {
        return new Vue({
            el: element
        });
    }
}