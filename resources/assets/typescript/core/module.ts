import { Vue } from 'av-ts';

export interface ModuleComponent {
    definition: typeof Vue,
    element: string
}

export abstract class Module {
    abstract components (): ModuleComponent[];

    constructor () {
        // call boot method
        this.boot();

        // register vue components
        this.registerComponents();
    }

    /**
     * register module components
     */
    protected registerComponents (): void {
        let components = {};

        this.components().forEach((moduleComponent: ModuleComponent) => {
            Vue.component(moduleComponent.element, moduleComponent.definition);
        });
    }

    /**
     * helper to define a {ModuleComponent} object
     *
     * @param element
     * @param definition
     * @returns {ModuleComponent}
     */
    protected static component(element: string, definition: typeof Vue): ModuleComponent {
        return <ModuleComponent>{
            definition: definition,
            element: element
        };
    }

    abstract boot() : void;
}