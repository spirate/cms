import { Bootloader } from './core/boot-loader'
import {
    BlogModule
} from './modules'

// register modules
Bootloader.registerModules([
    BlogModule
    // add other modules here
]);

// register vue root instance
Bootloader.registerVueRoot('#app');