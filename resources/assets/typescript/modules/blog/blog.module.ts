import { Module, ModuleComponent } from '../../core/module';
import {
    BlogPostCardComponent
} from './components'

export class BlogModule extends Module {
    public boot (): void {
        // ...
    }

    components(): ModuleComponent[] {
        return [
            Module.component('blog-post-card', BlogPostCardComponent)
        ];
    }
}