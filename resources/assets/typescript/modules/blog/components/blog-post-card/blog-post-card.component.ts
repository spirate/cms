import { Vue, Component, Lifecycle } from 'av-ts'

@Component({
    props: {},
    template: require('./blog-post-card.component.html')
})
export class BlogPostCardComponent extends Vue {
    /**
     * mounted lifecycle hook
     */
    @Lifecycle mounted () {

    }
}