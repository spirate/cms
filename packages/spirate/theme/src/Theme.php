<?php

namespace Spirate\Theme;

use ErrorException;
use Illuminate\Contracts\Routing\ResponseFactory;
use JsonSchema\Validator as JsonValidator;
use Spirate\Theme\Contracts\Theme as ThemeContract;
use Assets;

class Theme implements ThemeContract
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Spirate\Foundation\Application
     */
    protected $app;

    /**
     * @var \Spirate\Theme\Manager
     */
    private $manager;

    /**
     * @var \Spirate\Theme\ThemeManifest
     */
    protected $manifest;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var ThemeAssets
     */
    protected $assets;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var bool
     */
    protected $loaded = false;

    /**
     * Theme class constructor
     *
     * @param string $name
     * @param \Spirate\Theme\Manager $manager
     */
    public function __construct($name, Manager $manager)
    {
        $this->name = $name;
        $this->app = app();
        $this->manager = $manager;

        $this->manager->exists($this->name, true);
    }

    /**
     * Make a theme view
     *
     * @param $view
     * @param array $data
     * @return \Illuminate\Contracts\View\View
     */
    public function view($view, $data = [])
    {
        $this->content = $this->app->make('view')->make($view, $data)->render();

        return $this->app->make('view')->make('theme::layouts/main');
    }

    /**
     * Make a theme view response
     *
     * @param $view
     * @param array $data
     * @param int $status
     * @param array $headers
     * @return \Illuminate\Http\Response
     */
    public function response($view, $data = [], $status = 200, $headers = [])
    {
        $this->content = $this->app->make('view')->make($view, $data)->render();

        return $this->app->make(ResponseFactory::class)->view('theme::layouts/main', [], $status, $headers);
    }

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return \Spirate\Theme\ThemeManifest
     */
    public function manifest()
    {
        return $this->manifest;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function assets()
    {
        return $this->assets;
    }

    /**
     * @return bool
     */
    public function isLoaded()
    {
        return $this->loaded;
    }

    /**
     * set theme data
     */
    public function load()
    {
        if ($this->isLoaded()) return;

        // set path
        $this->path = $this->manager->getThemePath($this->name);

        // set manifest
        $this->manifest = $this->readManifest();

        /* set assets */

        // create assets object
        $this->assets = new ThemeAssets($this);

        // set as loaded
        $this->loaded = true;
    }

    /**
     * @return \Spirate\Theme\ThemeManifest
     * @throws ErrorException
     */
    protected function readManifest()
    {
        $manifest = $this->path . '/theme.json';

        if (!file_exists($manifest)) {
            throw new ErrorException(sprintf(
                'missing theme manifest file "theme.json" in %s',
                $this->path
            ));
        }

        $validator = new JsonValidator();
        $data = json_decode(file_get_contents($manifest));
        $schemaFile = realpath(dirname(dirname(__FILE__)) . '/resources/theme-manifest-schema.json');

        $validator->validate($data, (object)['$ref' => 'file://' . $schemaFile]);

        if (!$validator->isValid()) {
            $errors = [];

            foreach ($validator->getErrors() as $error) {
                $errors[] = sprintf("[%s] %s in theme manifest file '%s'.\n",
                    $error['property'], $error['message'], $manifest);
            }

            throw new ErrorException(implode("\n", $errors));
        }

        $themeManifest = new ThemeManifest();

        foreach($data as $property => $value)
        {
            $themeManifest->{$property} = $value;
        }

        $themeManifest->file = $manifest;

        return $themeManifest;
    }
}