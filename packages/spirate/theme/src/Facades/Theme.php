<?php

namespace Spirate\Theme\Facades;

use Illuminate\Support\Facades\Facade;
use Spirate\Theme\Contracts\Theme as ThemeContract;


class Theme extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ThemeContract::class;
    }
}