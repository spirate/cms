<?php

namespace Spirate\Theme\TwigExtensions;

use Spirate\Foundation\Application;
use Spirate\Theme\Theme;
use Spirate\Theme\Contracts\Theme as ThemeContract;
use Twig_Extension;
use Twig_SimpleFunction;

/*
 * TODO: remove this, better use ( $app['view']->share('theme_assets', function () { }) )
 */
class TwigViewExtension extends Twig_Extension
{
    /**
     * @var Application
     */
    protected $app;

    function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('theme_assets', [$this, 'themeAssets'], ['is_safe' => ['html']])
        );
    }

    public function themeAssets($types = [], $html = true, $separator = "\n\t")
    {
        /** @var Theme $theme */
        $theme = $this->app[ThemeContract::class];
        $assets = $theme->assets()->output($types, $html)->toArray();


        return implode($separator, $assets);
    }
}