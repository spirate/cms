<?php

namespace Spirate\Theme;

use Illuminate\Support\ServiceProvider;
use Spirate\Foundation\Application;
use Spirate\Theme\Contracts\Theme as ThemeContract;
use Spirate\Theme\TwigExtensions\TwigViewExtension;


class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the services.
     *
     * @return void
     */
    public function boot()
    {
        // settings...
    }

    /**
     * Register the services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerTheme();

        $this->registerViewExtensions();
    }

    /**
     * Register Theme app binding
     */
    public function registerTheme()
    {
        $this->app->singleton('themes', function (Application $app) {
            return new Manager($app);
        });

        $this->app->bind(ThemeContract::class, function () {
            return $this->app['themes']->current();
        });

    }

    protected function registerViewExtensions()
    {
        $this->app->singleton('theme.twig.extension', function (Application $app) {
            return new TwigViewExtension($app);
        });

        \Twig::addExtension($this->app->make('theme.twig.extension'));
    }
}