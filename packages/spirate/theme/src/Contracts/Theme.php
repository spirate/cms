<?php

namespace Spirate\Theme\Contracts;


interface Theme
{
    /**
     * @return \Spirate\Theme\ThemeAssets
     */
    public function assets();

    /**
     * @return \Spirate\Theme\ThemeManifest
     */
    public function manifest();
}