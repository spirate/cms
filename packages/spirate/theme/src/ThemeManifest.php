<?php

namespace Spirate\Theme;


/**
 * Class ThemeManifest
 *
 * @package Spirate\Theme
 * @author Spirate team <dev@spirate.net>
 *
 * @property string $name;
 * @property string $description;
 * @property \stdClass[] $authors;
 * @property string $version;
 * @property string $palette;
 * @property string $file;
 */
class ThemeManifest
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $version;

    /**
     * @var string
     */
    protected $authors;

    /**
     * @var string
     */
    protected $palette;

    /**
     * @var string
     */
    protected $file;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \stdClass[]
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * @param \stdClass[] $authors
     */
    public function setAuthors($authors)
    {
        $this->authors = $authors;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getPalette()
    {
        return $this->palette;
    }

    /**
     * @param string $palette
     */
    public function setPalette($palette)
    {
        $this->palette = $palette;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Magic setter method
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
            return;
        }

        trigger_error('property ' . $name . ' not exists in ' . __CLASS__, E_USER_ERROR);
    }

    /**
     * Magic getter method
     *
     * @param $name
     * @return mixed
     */
    function __get($name)
    {
        if (!property_exists($this, $name)) {
            trigger_error('property ' . $name . ' not exists in ' . __CLASS__, E_USER_ERROR);
        }

        return $this->{$name};
    }
}