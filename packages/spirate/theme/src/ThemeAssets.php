<?php

namespace Spirate\Theme;

use Illuminate\Support\Collection;
use Spirate\Assets\Asset;
use Spirate\Assets\AssetGroup;
use Assets;


class ThemeAssets extends AssetGroup
{
    /**
     * @var \Spirate\Theme\Theme
     */
    protected $theme;

    /**
     * ThemeAssets constructor.
     * @param Theme $theme
     */
    public function __construct(Theme $theme)
    {
        parent::__construct();

        $this->theme = $theme;

        $this->loadAssetsConfig();
    }

    /**
     * @param string $separator
     * @return string
     */
    public function stylesheets($separator = "\n\t")
    {
        return implode($separator, $this->output('css', true)->toArray());
    }

    /**
     * @param string $separator
     * @return string
     */
    public function scripts($separator = "\n\t")
    {
        return implode($separator, $this->output('js', true)->toArray());
    }

    /**
     * Load Assets configuration
     */
    protected function loadAssetsConfig()
    {
        $assets = Assets::createGroup();

        Assets::loadFromConfig(
            $this->theme->getPath() . '/assets.json',
            $assets
        );

        $this->collection = $this->collection->merge($assets->getCollection());
    }
}