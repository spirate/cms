<?php

namespace Spirate\Theme;

use Spirate\Foundation\Application;
use Spirate\Theme\Exceptions\ThemeNotFoundException;


class Manager
{
    /**
     * @var \Spirate\Foundation\Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var \Spirate\Theme\Theme
     */
    protected $theme;

    /**
     * @var \Spirate\Theme\Theme[]
     */
    protected $themes;

    /**
     * @var string
     */
    protected $themesPath;

    /**
     * Class constructor.
     *
     * @param \Spirate\Foundation\Application $app
     */
    function __construct(Application $app)
    {
        // set application instance
        $this->app = $app;

        // set theme config
        $this->config = $app['config']->get('theme');

        // initialize
        $this->initialize();
    }

    /**
     * set initial configuration
     */
    public function initialize()
    {
        // set themes path
        $this->themesPath = isset($this->config['themes_path'])
            ? $this->config['themes_path']
            : 'themes';

        // add theme alias asset path as callback
        app('assets')->addPathAlias(function () {
            return $this->theme->getPath() . '/assets';
        }, 'theme');

        // set default theme
        if (isset($this->config['default_theme'])) {
            $this->uses($this->config['default_theme']);
        }
    }

    /**
     * @return \Spirate\Theme\Theme
     */
    public function current()
    {
        return $this->theme;
    }

    /**
     * @return Theme[]
     */
    public function all()
    {
        return $this->themes;
    }

    /**
     * Check if a theme exists
     *
     * @param $theme
     * @param bool $throw
     * @return bool
     * @throws \Spirate\Theme\Exceptions\ThemeNotFoundException
     */
    public function exists($theme, $throw = false)
    {
        $exists = is_dir(realpath(base_path($this->themesPath . '/' . $theme)));

        if ($throw && !$exists) {
            throw new ThemeNotFoundException(
                'theme "' . $theme . '" not exists in path "' . $this->getThemesPath() . '"');
        }

        return $exists;
    }

    /**
     * Find a theme, if not exists throw an error
     *
     * @param $theme
     * @return Theme
     */
    public function findOrFail($theme)
    {
        $this->exists($theme, true);

        return $this->makeTheme($theme);
    }

    /**
     * Find a theme
     *
     * @param $theme
     * @return \Spirate\Theme\Theme|false
     */
    public function find($theme)
    {
        return $this->exists($theme) || $this->makeTheme($theme);
    }

    /**
     * Set theme
     *
     * @param string|Theme $theme
     */
    public function uses($theme)
    {
        if ($theme instanceof Theme) {
            $this->theme = $theme;
        } else {
            $this->theme = $this->findOrFail($theme);
        }

        // load theme
        $this->theme->load();

        // set view vars
        $this->app->make('view')->share('Theme', $this->theme);
        $this->app->make('view')->replaceNamespace('theme', $this->theme->getPath());
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return string
     */
    public function getThemesPath()
    {
        return realpath(isset($this->config['themes_path'])
            ? base_path($this->config['themes_path'])
            : base_path('themes'));
    }

    /**
     * Get theme absolute path
     *
     * @param $theme
     * @param bool $check
     * @return string
     */
    public function getThemePath($theme, $check = true)
    {
        !$check || $this->exists($theme, true);

        return realpath(base_path($this->themesPath . '/' . $theme));
    }

    /**
     * @param $theme
     * @return Theme
     */
    protected function makeTheme($theme)
    {
        if (!isset($this->themes[$theme])) {
            $this->themes[$theme] = new Theme($theme, $this);
        }

        return $this->themes[$theme];
    }
}