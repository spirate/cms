<?php

namespace Spirate\Widgets;

use Illuminate\Support\Collection;
use Spirate\Widgets\Contracts\WidgetGroup as WidgetGroupContract;


class WidgetGroup implements WidgetGroupContract
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * WidgetGroup constructor.
     */
    public function __construct()
    {
        $this->collection = new Collection();
    }

    /**
     * @param AbstractWidget $widget
     */
    public function add(AbstractWidget $widget)
    {
        $this->collection->push($widget);
    }

    /**
     * @param $widget
     */
    public function remove($widget)
    {
        
    }
}