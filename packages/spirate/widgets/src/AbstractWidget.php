<?php

namespace Spirate\Widgets;

use View, Twig;

// TODO: add contract AbstractWidgetContract
abstract class AbstractWidget
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var string
     */
    private $view;

    /**
     * @param array $params
     */
    public final function __construct(array $params = [])
    {
        // set params
        $this->params = $params;

        // set widget view
        $this->setViewPath();

        // call init method
        call_user_func([$this, 'init'], $this->config, $params);
    }

    /**
     * @return \Illuminate\Contracts\View\View|string
     */
    public final function run()
    {
        if (file_exists($this->view)) {
            $view = View::file($this->view, $this->data);
        } else {
            $view = View::make($this->view, $this->data);
        }

        return $view;
    }

    public function getPath()
    {
        return dirname((new \ReflectionClass($this))->getFileName());
    }

    abstract public function init();

    protected final function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    private function setViewPath()
    {
        // find custom view
        if (method_exists($this, 'view')) {
            $view = $this->view();

            if (View::exists($view)) {
                $this->view = $view;
            } else if (file_exists($view)) {
                $this->view = realpath($view);
            } else {
                $this->view = get_class($this) . '::' . $view;
            }
        } else {
            $this->view = get_class($this) . '::index';
        }
    }
}