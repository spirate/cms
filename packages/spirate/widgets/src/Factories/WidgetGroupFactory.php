<?php

namespace Spirate\Widgets\Factories;

use Spirate\Widgets\Contracts\WidgetGroupFactory as WidgetGroupFactoryContract;
use Spirate\Widgets\WidgetGroup;


class WidgetGroupFactory implements WidgetGroupFactoryContract
{
    public function create($name)
    {
        return new WidgetGroup($name);
    }
}