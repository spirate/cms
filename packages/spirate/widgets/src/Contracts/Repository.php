<?php

namespace Spirate\Widgets\Contracts;


interface Repository
{
    public function run($expression, array $params = [], $create = true);
    public function get($expression, array $params = []);
    public function create($expression, array $params = []);
    public function exists($expression);
    public function createGroup($name);
}