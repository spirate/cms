<?php

namespace Spirate\Widgets\Contracts;

use Spirate\Widgets\AbstractWidget;


interface WidgetGroup
{
    public function add(AbstractWidget $widget);
    public function remove($widget);
}