<?php

namespace Spirate\Widgets\Facades;

use Illuminate\Support\Facades\Facade;
use Spirate\Widgets\Contracts\Repository as RepositoryContract;


class Widget extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return RepositoryContract::class;
    }
}