<?php

namespace Spirate\Widgets;

use Spirate\Foundation\Application;
use Spirate\Widgets\Contracts\Repository as RepositoryContract;
use Spirate\Widgets\Contracts\WidgetGroupFactory as WidgetGroupFactoryContract;
use Module, View;


class Repository implements RepositoryContract
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var \Spirate\Widgets\AbstractWidget[]
     */
    protected $instances;

    function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Run a widget
     *
     * @param $expression
     * @param array $params
     * @param bool $create
     * @return mixed
     */
    public function run($expression, array $params = [], $create = true)
    {
        return $this->{$create ? 'create' : 'get'}($expression, $params)->run();
    }

    /**
     * Get a widget instance
     *
     * @param null $expression
     * @param array $params
     * @return AbstractWidget
     * @throws \ErrorException
     */
    public function get ($expression, array $params = [])
    {
        $widgetClass = $this->parseExpression($expression);

        if (!isset($this->instances[$widgetClass])) {
            return $this->create($expression, $params);
        }

        return $this->instances[$widgetClass];
    }

    /**
     * Create a widget instance
     *
     * @param $expression
     * @param array $params
     * @return AbstractWidget
     * @throws \ErrorException
     */
    public function create($expression, array $params = [])
    {
        if (empty($expression)) {
            throw new \ErrorException('can\'t run widget, expression is required.');
        }

        $widgetClass = $this->parseExpression($expression);

        if (!$this->exists($expression)) {
            throw new \ErrorException(sprintf("widget '%s' not found.", $widgetClass));
        }

        if (!is_subclass_of($widgetClass, AbstractWidget::class)) {
            throw new \ErrorException(sprintf(
                'Widget class "%s" must extends "%s" class.', $widgetClass, AbstractWidget::class));
        }

        /** @var \Spirate\Widgets\AbstractWidget $instance */
        $instance = new $widgetClass($params);

        if (!isset($this->instances[$widgetClass])) {
            /* add views widget path to twig loader */

            /** @var \Twig_Loader_Filesystem $twigLoader */
            $twigLoader = app()->make('widgets.twig.loader');
            $twigLoader->addPath($instance->getPath() . '/views');

            // register view namespace path
            View::addNamespace(get_class($instance), $instance->getPath() . '/views');

            // add instance to list
            $this->instances[$widgetClass] = $instance;
        }

        return new $widgetClass($params);
    }

    public function exists($expression)
    {
        return class_exists($this->parseExpression($expression));
    }

    /**
     * @param $name
     * @return \Spirate\Widgets\WidgetGroup
     */
    public function createGroup($name)
    {
        return app(WidgetGroupFactoryContract::class)->create($name);
    }

    private function parseExpression($expression)
    {
        if (class_exists($expression) && is_subclass_of($expression, AbstractWidget::class)) {
            return ltrim($expression, '\\');
        } else if (strpos($expression, '::') !== false) {
            list($moduleAlias, $widget) = explode('::', $expression);

            // check module exists
            $module = Module::findOrFail($moduleAlias);
            $modulesNamespace = config('modules.namespace', 'Modules');

            // set widget class name
            $widgetClass = implode('\\', [
                $modulesNamespace,
                $module->getName(),
                'Widgets',
                $widget,
                $widget
            ]);

            return ltrim($widgetClass, '\\');
        }

        return strpos($expression, '\\') !== 0
            ? config('widgets.namespace', 'Widgets') . '\\' . $expression
            : $expression;
    }
}