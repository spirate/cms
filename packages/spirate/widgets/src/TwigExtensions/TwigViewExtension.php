<?php

namespace Spirate\Widgets\TwigExtensions;

use Spirate\Foundation\Application;
use Twig_Extension;
use Twig_SimpleFunction;

class TwigViewExtension extends Twig_Extension
{
    /**
     * @var Application
     */
    protected $app;

    function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('widget', [$this, 'widget'], ['is_safe' => ['html']]),
        );
    }

    public function widget($expression, array $params = [])
    {
        return $this->app->make('widgets')->run($expression, $params);
    }
}