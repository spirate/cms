<?php

namespace Spirate\Widgets\Providers;

use Illuminate\Support\ServiceProvider;
use Spirate\Foundation\Application;
use Spirate\Widgets\Factories\WidgetGroupFactory;
use Spirate\Widgets\Repository;
use Spirate\Widgets\Contracts\WidgetGroupFactory as WidgetGroupFactoryContract;
use Spirate\Widgets\Contracts\Repository as RepositoryContract;
use Spirate\Widgets\TwigExtensions\TwigViewExtension;
use Blade, Twig, View;


class WidgetsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the services.
     *
     * @return void
     */
    public function boot()
    {
        // TODO: stubs, config, loadViewsFrom(..., namespace ), $this->publishes(...)
        // TODO: crear una clase WidgetGrid, esta servira para WidgetGroup:
        // WidgetGrid::make( columns|rows|template|classes(css) ) cache or database widgets
    }

    /**
     * Register the services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();

        $this->registerViewExtensions();
    }

    protected function registerServices()
    {
        $this->app->singleton(RepositoryContract::class, function (Application $app) {
            return new Repository($app);
        });

        $this->app->singleton(WidgetGroupFactoryContract::class, function (Application $app) {
            return new WidgetGroupFactory();
        });

        $this->app->alias(RepositoryContract::class, 'widgets');
        $this->app->alias(WidgetGroupFactoryContract::class, 'widgets.group');

        // twig services
        $this->app->singleton('widgets.twig.loader', \Twig_Loader_Filesystem::class);

        $this->app->singleton('widgets.twig.extension', function ($app) {
            return new TwigViewExtension($app);
        });
    }

    protected function registerViewExtensions()
    {
        // blade integration
        Blade::directive('widget', function ($expression) {
            return  '<?php echo app("widgets")->run(' . $expression . '); ?>';
        });

        // twig integration
        Twig::addExtension($this->app->make('widgets.twig.extension'));

        $this->app['twig.loader']->addLoader($this->app['widgets.twig.loader']);

        // register views namespace path
        View::addNamespace('widgets', base_path('/resources/views/widgets'));
    }
}
