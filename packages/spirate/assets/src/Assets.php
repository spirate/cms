<?php

namespace Spirate\Assets;

use ErrorException;
use Illuminate\Support\Collection;
use JsonSchema\Validator as JsonValidator;
use Spirate\Foundation\Application;
use Spirate\Assets\Contracts\AssetGroupFactory as AssetGroupFactoryContract;


class Assets
{
    /**
     * @var \Spirate\Foundation\Application
     */
    protected $app;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $pathAliases;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $groups;

    /**
     * @var \Spirate\Assets\AssetGroup
     */
    protected $assets;

    /**
     * @var Asset[]
     */
    protected $types = [
        Types\CssAsset::class,
        Types\JSAsset::class
    ];

    /**
     * Assets constructor.
     * @param Application $app
     */
    function __construct(Application $app)
    {
        $this->app = $app;

        $this->groups = new Collection();

        $this->pathAliases = new Collection();

        $this->initialize();
    }

    /**
     * Add a path
     *
     * @param string|callable $path
     * @param string $alias
     * @throws \ErrorException
     */
    public function addPathAlias($path, $alias)
    {
        if (!preg_match('~^[a-z0-9-_]+$~i', $alias)) {
            throw new ErrorException(sprintf('invalid alias format "%s"', $alias));
        }

        if (is_callable($path)) {
            $this->pathAliases->put($alias, $path);
            return;
        }

        if (!file_exists(realpath($path))) {
            throw new ErrorException(sprintf('path "%s" not exists', $path));
        }

        $this->pathAliases->put($alias, realpath($path));
    }

    public function getPathAlias($alias)
    {
        if (!$this->pathAliases->keys()->contains($alias)) {
            throw new ErrorException('Error getting path alias, alias ' . $alias . ' is undefined');
        }

        $path = $this->pathAliases->get($alias);

        if (is_callable($path)) {
            $path = realpath(call_user_func($path));

            if (!file_exists($path)) {
                throw new ErrorException(sprintf('path "%s" not exists', $path));
            }
        }

        return $path;
    }

    /**
     * @param string|null $name
     * @param array $assets
     * @return AssetGroup
     */
    public function createGroup($name = null, array $assets = [])
    {
        $group = $this->app
            ->make(AssetGroupFactoryContract::class)
            ->create($assets, $name);

        $this->groups->push($group);

        return $group;
    }

    public function createAsset($type, $path, $name = null)
    {
        $this->checkAssetType($type);

        if (!class_exists($type)) {
            $assetTypeClass = $this->getAssetTypeClass($type);
        } else {
            $assetTypeClass = $type;
        }

        $asset = new $assetTypeClass($path, $name);

        return $asset;
    }

    /**
     * @param $source
     * @return bool|string
     * @throws ErrorException
     */
    public function findAssetPath($source)
    {
        $matchFormat = preg_match('~^([a-z0-9-_]+):\/\/~i', $source, $match);

        if ($matchFormat && ($pathAlias = $this->getPathAlias($match[1]))) {
            $path = str_replace($match[0], $pathAlias . '/', $source);
        } else {
            $path = $source;
        }

        if (!file_exists($path)) {
            throw new ErrorException(sprintf('asset %s not found.', $path));
        }

        return realpath($path);
    }

    /**
     * initialize config
     */
    protected function initialize()
    {
        // create global assets group
        $this->assets = $this->createGroup('__default__');
    }

    /**
     * @param $type
     * @return string
     * @throws ErrorException
     */
    public function getAssetTypeClass($type)
    {
        foreach ($this->types as $typeClass) {
            if ($type === $typeClass::extension()) {
                return $typeClass;
            }
        }

        throw new ErrorException(sprintf('Asset type "%s" not defined.', $type));
    }

    public function getAssetTypes()
    {
        return $this->types;
    }

    public function addAssetType($assetType)
    {
        $this->checkAssetType($assetType);

        $this->types[] = $assetType;
    }

    /**
     * @param array|string $config
     * @param \Spirate\Assets\AssetGroup|string|null $group
     * @return \Spirate\Assets\AssetGroup
     */
    public function loadFromConfig($config, $group = null)
    {
        if (isset($group)) {
            if ($group instanceof AssetGroup) {
                $refgroup = $group;
            } else {
                $refgroup = $this->createGroup($group);
            }
        } else {
            $refgroup = $this->assets;
        }

        $parsed = $this->parseConfig($config);

        // add assets
        foreach ($parsed->assets as $assetInfo) {
            $class = $this->getAssetTypeClass($assetInfo->type);
            $asset = new $class($assetInfo->src);

            $refgroup->add($asset);
        }

        /*
         * TODO: agregar en assets.json la posiblidad de crear grupos por "path glob"
         * ejemplo:
         *
         * {
         *      "groups": {
         *          "theme-css": ["css/*.css", "vendor/bootstrap-css/*.css"]
         *      }
         * }
         *
         */

        /*
         * TODO: agregar un interceptor de rutas parar aplicar las reglas de los assets
         *
         * "routes": {
         *      "/blog" {
         *          "exclude": ["jquery"]
         *      }
         * }
         */

        return $refgroup;
    }

    protected function checkAssetType($type)
    {
        !class_exists($type) && ($type = $this->getAssetTypeClass($type));

        if ((new \ReflectionClass($type))->getParentClass()->name !== Asset::class) {
            throw new \ErrorException(
                'Invalid Asset Type class "' . $type . '", class must be extends ' . Asset::class);
        }
    }

    /**
     * @param string|array $config
     * @return mixed
     * @throws ErrorException
     */
    protected function parseConfig($config)
    {
        // set validator
        $validator = new JsonValidator();

        if (is_string($config) && is_file($config)) {
            if (!file_exists($config)) {
                throw new ErrorException(sprintf('missing asset configuration file "%s"', $config));
            }

            // set config
            $data = json_decode(file_get_contents($config));
        } else {
            $data = json_decode(json_encode($config), false);
        }

        // validate config schema
        $schemaFile = dirname(dirname(__FILE__)) . '/resources/assets-config-schema.json';
        $validator->validate($data, (object)['$ref' => 'file://' . realpath($schemaFile)]);

        if (!$validator->isValid()) {
            $errors = [];

            foreach ($validator->getErrors() as $error) {

                if (is_string($config) && is_file($config)) {
                    $errors[] = sprintf("%s in config file '%s'.\n",
                        $error['message'], $config);
                } else {
                    $errors[] = sprintf("%s in config object \n\n%s.\n",
                        $error['message'], json_encode($config));
                }
            }

            throw new ErrorException(implode("\n", $errors));
        }

        // add config file source path
        if (is_string($config) && is_file($config)) {
            $data->__configFile = $config;
        }

        return $data;
    }
}