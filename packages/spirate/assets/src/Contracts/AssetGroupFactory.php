<?php

namespace Spirate\Assets\Contracts;


interface AssetGroupFactory
{
    public function create(array $assets = [], $name = null);
}