<?php

namespace Spirate\Assets\Contracts;


interface Asset
{
    /**
     * @return string
     */
    static function extension();
}