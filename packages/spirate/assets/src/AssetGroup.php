<?php

namespace Spirate\Assets;

use Illuminate\Support\Collection;
use Psy\Exception\ErrorException;
use Spirate\Assets\Contracts\Asset as AssetContract;
use Assets;


class AssetGroup implements \ArrayAccess
{
    const OUTPUT_ALL = 0x00000001;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * AssetGroup constructor.
     * @param array $assets
     * @param string|null $name
     */
    public function __construct(array $assets = [], $name = null)
    {
        // set collection
        $this->collection = new Collection();

        // add initial assets to collection
        if (!empty($assets)) {
            /** @var Asset $asset */
            foreach ($assets as $asset) {
                $this->add($asset);
            }
        }

        // set name
        $this->name = $name;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function add(Asset $asset)
    {
        $this->collection->push($asset);
    }

    /**
     * @param Asset[] $assets
     */
    public function addAll(array $assets = [])
    {
        foreach ($assets as $asset) {
            $this->add($asset);
        }
    }

    public function remove(Asset $asset)
    {
        $this->collection = $this->collection->reject(function (Asset $forgetAsset) use ($asset) {
            return $asset === $forgetAsset;
        });
    }

    public function removeAll($assets)
    {
        foreach($assets as $asset) {
            $this->remove($asset);
        }
    }

    public function filter(callable $callback)
    {
        return $this->collection->filter($callback);
    }

    public function findByType($type)
    {
        $assetType = class_exists($type) ? $type : (Assets::getAssetTypeClass($type));

        $output = $this->collection->filter(function (Asset $asset) use ($assetType) {
            return ($asset instanceof $assetType);
        });

        return $output;
    }

    /**
     * @param int|array $types
     * @return \Illuminate\Support\Collection
     */
    public function findByTypes($types = [])
    {
        $collection = new Collection();

        foreach ((array) $types as $typeClass) {
            $assets = $this->findByType($typeClass);

            foreach($assets as $asset) {
                $collection->push($asset);
            }
        }

        return $collection;
    }

    /**
     * @param int $types
     * @param bool $html
     * @return \Illuminate\Support\Collection
     */
    public function output($types = self::OUTPUT_ALL, $html = false)
    {
        $assets = $this->findByTypes($types === self::OUTPUT_ALL ? Assets::getAssetTypes() : $types);

        $output = $assets->map(function (Asset $asset) use ($html) {
            return $asset->{$html ? 'html' : 'url'}();
        });

        return $output;
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return $this->collection->offsetExists($offset);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->collection->offsetGet($offset);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->collection->offsetSet($offset, $value);
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        $this->collection->offsetUnset($offset);
    }
}