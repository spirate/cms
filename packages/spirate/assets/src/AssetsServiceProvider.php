<?php

namespace Spirate\Assets;

use Illuminate\Support\ServiceProvider;
use Spirate\Assets\Contracts\AssetGroupFactory as AssetGroupFactoryContract;
use Spirate\Assets\Factories\AssetGroupFactory;
use Spirate\Foundation\Application;


class AssetsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the services.
     *
     * @return void
     */
    public function boot()
    {
        // settings...
    }

    /**
     * Register the services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AssetGroupFactoryContract::class, function () {
            return new AssetGroupFactory();
        });

        $this->app->alias(AssetGroupFactoryContract::class, 'assets.factory');

        $this->app->singleton('assets', function (Application $app) {
            return new Assets($app);
        });
    }
}