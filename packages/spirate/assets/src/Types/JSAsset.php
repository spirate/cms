<?php

namespace Spirate\Assets\Types;

use Spirate\Assets\Asset;


class JSAsset extends Asset
{
    protected function htmlTag()
    {
        return [
            'tag' => 'script',
            'attrs' => [
                'type' => 'text/javascript',
                'src' => '%url%'
            ]
        ];
    }

    public static function extension()
    {
        return 'js';
    }
}