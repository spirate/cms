<?php

namespace Spirate\Assets\Types;

use Spirate\Assets\Asset;


class CssAsset extends Asset
{
    /**
     * @return array
     */
    protected function htmlTag()
    {
        return [
            'tag' => 'link',
            'attrs' => [
                'rel' => 'stylesheet',
                'href'=> '%url%',
                'type' => 'text/css'
            ],
            'self_close' => true
        ];
    }

    public static function extension()
    {
        return 'css';
    }
}