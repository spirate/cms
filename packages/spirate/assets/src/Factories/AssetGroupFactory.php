<?php

namespace Spirate\Assets\Factories;

use Spirate\Assets\AssetGroup;
use Spirate\Assets\Contracts\AssetGroupFactory as AssetGroupFactoryContract;


class AssetGroupFactory implements AssetGroupFactoryContract
{
    /**
     * Create a new asset group
     *
     * @param string $name asset group name
     * @return \Spirate\Assets\AssetGroup
     */
    public function create(array $assets = [], $name = null)
    {
        return new AssetGroup($assets, $name);
    }
}