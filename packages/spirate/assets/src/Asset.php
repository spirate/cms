<?php

namespace Spirate\Assets;

use Spirate\Assets\Contracts\Asset as AssetContract;


abstract class Asset implements AssetContract
{
    protected $path;

    public function __construct($path, $name = null)
    {
        $this->path = app('assets')->findAssetPath($path);
    }

    public function html()
    {
        $htmlTag = $this->htmlTag();

        $tag = e($htmlTag['tag']);
        $attrs = [];
        $selfClose = isset($htmlTag['self_close']) && ((bool) $htmlTag['self_close']);
        $replaces = [
            '%path%' => $this->path,
            '%url%' => $this->url()
        ];

        foreach($htmlTag['attrs'] as $attr => $value) {
            $valueFiltered = e(str_replace(array_keys($replaces), array_values($replaces), $value));
            $attrs[] = e($attr) . '="' . $valueFiltered . '"';
        }

        return implode('', [
            '<',
            $tag,
            ' ',
            implode(' ', $attrs),
            (!$selfClose ? '></' . $tag . '>' : ' />')
        ]);
    }

    public function url()
    {
        return $this->urlFromPath($this->path);
    }

    abstract protected function htmlTag();

    protected function urlFromPath($path)
    {
        $url = str_replace([base_path(), '\\'], [app()->make('url')->to('/'), '/'], $path);

        return $url;
    }
}