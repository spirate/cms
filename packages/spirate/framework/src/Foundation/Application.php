<?php

namespace Spirate\Foundation;

use Illuminate\Foundation\Application as BaseApplication;
use Module;


class Application extends BaseApplication
{
    public function __construct($basePath = null)
    {
        parent::__construct($basePath);
    }

    public function getNamespace()
    {
        return config('modules.namespace', 'Modules') . '\\' . Module::findOrFail('Core')->getName();
    }
}