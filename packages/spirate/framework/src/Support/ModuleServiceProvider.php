<?php

namespace Spirate\Support;

use Illuminate\Support\ServiceProvider;


abstract class ModuleServiceProvider extends ServiceProvider
{
    public function __construct($app)
    {
        parent::__construct($app);

        $this->registerWidgets();
    }

    protected function registerWidgets()
    {

    }

    /**
     * @return array
     */
    abstract public function widgets();
}